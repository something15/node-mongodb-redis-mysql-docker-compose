const express = require('express');
const mongoose = require('mongoose');

const app = express();

app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: false }));

var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Connect to MongoDB
// mongoose
//   .connect(
//     'mongodb://mongo:27017/docker-node-mongo',
//     { useNewUrlParser: true }
//   )
//   .then(() => console.log('MongoDB Connected'))
//   .catch(err => console.log(err));

// const Item = require('./models/Item');

// app.get('/', (req, res) => {
//   Item.find()
//     .then(items => res.render('index', { items }))
//     .catch(err => res.status(404).json({ msg: 'No items found' }));
// });

// app.post('/item/add', (req, res) => {
//   const newItem = new Item({
//     name: req.body.name
//   });

//   newItem.save().then(item => res.redirect('/'));
// });

const mongoClient = require('mongodb').MongoClient;
const mongoUrl = "mongodb://mongo:27017";

const mongoDb = "testDb";
const mongoCol = "testCol";

app.get("/mongo", async function (req, res) {
  let curClient = null;
  try {
    curClient = await mongoClient.connect(mongoUrl);
    console.log("mongo数据库已连接");
    const curCol = curClient.db(mongoDb).collection(mongoCol);
    const rst = await curCol.find().toArray();
    res.json(rst);
  } catch (err) {
    console.log("mongo", err);
    res.json(err);
  } finally {
    if (curClient != null) curClient.close();
  }
})

app.post("/mongo/update", async function (req, res) {
  let curClient = null;
  try {
    curClient = await mongoClient.connect(mongoUrl);
    console.log("mongo数据库已连接");
    const curCol = curClient.db(mongoDb).collection(mongoCol);
    await curCol.updateOne({}, { $set: { "data": req.body.data } }, { upsert: true });
    res.json("更新成功");
  } catch (err) {
    console.log("mongo", err);
    res.json(err);
  } finally {
    if (curClient != null) curClient.close();
  }
})

const { promisify } = require("util");
const redisConn = require("redis");
const redisClient = redisConn.createClient(6379, "redis");

redisClient.on("error", function (error) {
  console.log(error);
});

const getAsync = promisify(redisClient.get).bind(redisClient);
const setAsync = promisify(redisClient.set).bind(redisClient);

// client.auth("123456"); 
app.get("/redis", async function (req, res) {
  try {
    const param = req.query;
    const key = param.name;
    const rst = await getAsync(key);
    res.json(rst);
  } catch (err) {
    console.log("redis", err);
    res.json(err);
  }
})

app.post("/redis/set", async function (req, res) {
  try {
    const key = req.body.name;
    const value = req.body.value;
    // console.log("req", key, value);
    await setAsync(key, value);
    res.json("设置成功");
  } catch (err) {
    console.log("redis", err);
    res.json(err);
  }
})


const mariadb = require('mariadb');
const pool = mariadb.createPool({
  host: 'mysql',
  user: 'root',
  password: 'root',
  connectionLimit: 5,
  port: '3306',
  allowPublicKeyRetrieval: true,
});

app.get("/mysql", async function (req, res) {
  let conn;
  try {
    conn = await pool.getConnection();
    await conn.query("use testData");
    querySql = "SELECT * FROM testTable";
    const rst = await conn.query(querySql);
    res.json(rst);
  } catch (err) {
    console.log("mysql", err);
    res.json(err);
  } finally {
    if (conn) return conn.end();
  }
})

app.post("/mysql/insert", async function (req, res) {
  let conn;
  try {
    const name = req.body.name;
    const age = req.body.age;
    conn = await pool.getConnection();
    await conn.query("use testData");
    addSql = "INSERT INTO testTable(name, age) VALUES(?, ?)";
    await conn.query(addSql, [name, age]);
    res.json("插入成功");
  } catch (err) {
    console.log("mysql", err);
    res.json(err);
  } finally {
    if (conn) return conn.end();
  }
})


const axios = require('axios');

app.put("/es/create_index", async function (req, res) {
  try {
    const url = "http://elasticsearch:9200/" + req.body.index;
    const rst = await axios.put(url, {});
    // console.log("es create rst", rst);
    res.json("es 新建index成功");
  } catch (err) {
    res.json(err);
  }
})

app.get("/es/get_index", async function (req, res) {
  try {
    const url = "http://elasticsearch:9200/_cat/indices?v";
    const rst = await axios.get(url);
    // console.log("es get rst: ", rst);
    res.json(rst.data);
  } catch (err) {
    res.json(err);
  }
})

app.delete("/es/delete_index", async function (req, res) {
  try {
    const url = "http://elasticsearch:9200/" + req.body.index;
    const rst = await axios.delete(url, {});
    // console.log("es delete rst: ", rst);
    res.json("es 删除index成功");
  } catch (err) {
    res.json(err);
  }
})


const port = 3000;

app.listen(port, () => console.log('Server running...'));
