将代码clone到本地之后，执行以下操作步骤：
1. 运行命令 docker-compose up -d --build --remove-orphans 启动容器；
2. 进入mongo容器，设置需要的数据库和集合：testDb，testCol；
3. 进入mysql容器，设置需要的数据库和集合：testData，testTable。 testTable中包两个字段name和age，类型分别为varchar和int；
4. 使用postman 执行以下请求：
   mongo：
   get： http://localhost/mongo，
   post：http://localhost/mongo/update，
   post请求参数：
   {
	"data": {
		"name": "infi",
		"value": "lyn"
	    }
    }
    
    mysql:
    get: http://localhost/mysql,    
    post: http://localhost/mysql/insert,
    post请求参数：
    {
	"name": "infi",
	"age": 35
    }
    
    redis:
    get: http://localhost/redis?name=infi,
    post: http://localhost/redis/set,
    post请求参数：
    {
		"name": "infi",
		"value": "laohu"
    }
  
    elasticsearch:
    get: http://localhost/es/get_index,
    put: http://localhost/es/create_index,
    put请求参数：
    {
	  "index": "customer2"
    }
    delete: http://localhost/es/delete_index
    delete请求参数：
    {
	  "index": "customer2"
    }

注：2,3两步也可以使用mongo，mysql的客户端实现，端口和密码配置参考docker-compose.yml文件。
